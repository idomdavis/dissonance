# dissonance

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/dissonance/main?style=plastic)](https://bitbucket.org/idomdavis/dissonance/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/dissonance?style=plastic)](https://bitbucket.org/idomdavis/dissonance/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/dissonance?style=plastic)](https://bitbucket.org/idomdavis/dissonance/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/dissonance)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

Dissonance is a Discord bridge written in Go
